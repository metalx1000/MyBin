#!/bin/bash
###################################################################### 
#Copyright (C) 2018  Kris Occhipinti
#https://filmsbykris.com

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
###################################################################### 

function main(){
  check $@ 
  upload
}

function check(){
  local OPTIND opt i
  while getopts ":qcni:" opt; do
    case $opt in
      i) img="$OPTARG";;
      c) c=true;; #put link in clipboard
      q) qr=true;; # display qrcode (qrencode needed)
      n) n=true ;; # Send notification to desktop
      \?) help;exit 1 ;; # Handle error: unknown option or missing required argument.
    esac
  done
  shift $((OPTIND -1))

  if [ "$img" = "" ]
  then 
    echo "No input file"
    exit 1
  fi
  mime="$(file --mime-type $img|awk '{print $2}')"
  url="http://magaimg.net/"
}

function upload(){
  link="$(curl -s -F "img[]=@$img;type=$mime" -D - "$url"|grep '^Location'|awk '{print $2}')"
  echo "$link"

  [ $qr ] && qrencode "$link" -t UTF8 
  [ $n ] && notify-send -t 3000 "Image uploaded" "$link"

  #put link in clipboards
  if [ $c ];
  then 
    echo "$link"|xclip
    echo "$link"|xclip -selection clipboard
  fi

  exit 0
}

function help(){
  cat << EOF
  Uploads an image to http://magaimg.net
  Usage: $0 -cnqi <file>
  Example: $0 -cnqi file.jpg

  -i <file>   Input image file
  -c          Copy link to clipboard
  -q          Display link qrcode (qrencode required)
  -n          Send Notification to Desktop
EOF
}

main $@

