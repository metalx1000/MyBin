#!/bin/bash
###################################################################### 
#Copyright (C) 2020  Kris Occhipinti
#https://filmsbykris.com

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
###################################################################### 

name="$(cat names.lst | fzf --prompt "Please Select a Name: " )"

[[ "$name" == "" ]] && echo -e "Invalid Selection\nGoodbye..." && exit 1

echo "You chose '$name'"
read -n1 -p 'Is this correct? [Y/n]: ' a
echo ""

[[ "$a" == "n" ]] || [[ "$a" == "N" ]] && echo "GoodBye..." && exit 1

echo "You selected '$name'"
