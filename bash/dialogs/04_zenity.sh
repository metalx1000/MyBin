#!/bin/bash
######################################################################
#Copyright (C) 2020  Kris Occhipinti
#https://filmsbykris.com

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
######################################################################

name="$(cat names.lst| zenity --list --title "People" --text "Please Select a Name" --column "Names")"

[[ "$name" == "" ]] && zenity --warning --width=400 --height=200 --text "Invalid Selection." && exit 1

zenity --question --text "You chose '$name'. Is this correct?" --no-wrap --ok-label "Yes" --cancel-label "No" || exit 1

zenity --info --width=400 --height=200 --text "You selected '$name'"
