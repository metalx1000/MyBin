#!/bin/bash
###################################################################### 
#Copyright (C) 2020  Kris Occhipinti
#https://filmsbykris.com

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
###################################################################### 

name="$(dialog --radiolist "Select items:" 0 0 0 $(sed 's/ /_/g;s/$/ . ./g' names.lst) --stdout)"

[[ "$name" == "" ]] && dialog --msgbox "No Names Selected" 0 0 && exit 1

name="$(echo $name|sed 's/_/ /g')"

dialog --default-button "yes" --yesno "You chose '$name'? Is this correct?" 0 0

[[ "$?" == "1" ]] && dialog --timeout 3 --msgbox "GoodBye..." 0 0 && exit 1

dialog --msgbox "Name Selected is '$name'" 0 0 

