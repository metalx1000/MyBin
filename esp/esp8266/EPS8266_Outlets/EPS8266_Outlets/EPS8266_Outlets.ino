#include <WiFiManager.h> // https://github.com/tzapu/WiFiManager
#include <ESP8266WebServer.h>
#include <RCSwitch.h>

/*
 * Copyright Kris Occhipinti March, 6th 2021
 * https://filmsbykris.com
 *This program is free software: you can redistribute it and/or modify                                 
 *it under the terms of the GNU General Public License as published by                                 
 *the Free Software Foundation, either version 3 of the License, or                                    
 *(at your option) any later version.                                                                  
 *                                                                                                     
 *This program is distributed in the hope that it will be useful,                                      
 *but WITHOUT ANY WARRANTY; without even the implied warranty of                                       
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                        
 *GNU General Public License for more details.                                                         
 *                                                                                                     
 *You should have received a copy of the GNU General Public License                                    
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
RCSwitch mySwitch = RCSwitch();

ESP8266WebServer server(80);


void setup() {
    WiFi.mode(WIFI_STA); // explicitly set mode, esp defaults to STA+AP

    // put your setup code here, to run once:
    Serial.begin(115200);

    //transmit 0 --> pin D3 on Wemo Esp
    mySwitch.enableTransmit(0);

    //This needs to be set to 180 for zap outlets
    mySwitch.setPulseLength(180);
    
    // WiFi.mode(WiFi_STA); // it is a good practice to make sure your code sets wifi mode how you want it.

    //WiFiManager, Local intialization. Once its business is done, there is no need to keep it around
    WiFiManager wm;

    //reset settings - wipe credentials for testing
    //wm.resetSettings();

    // Automatically connect using saved credentials,
    // if connection fails, it starts an access point with the specified name ( "AutoConnectAP"),
    // if empty will auto generate SSID, if password is blank it will be anonymous AP (wm.autoConnect())
    // then goes into a blocking loop awaiting configuration and will return success result

    bool res;
    // res = wm.autoConnect(); // auto generated AP name from chipid
    res = wm.autoConnect("Outlets"); // anonymous ap
    //res = wm.autoConnect("AutoConnectAP","password"); // password protected ap

    if(!res) {
        Serial.println("Failed to connect");
        // ESP.restart();
    } 
    else {
        //if you get here you have connected to the WiFi    
        Serial.println("connected...yeey :)");
    }

  server.on("/", handleRoot);
  server.begin();
}


void handleRoot() {
  String msg = "";
  
  for (uint8_t i = 0; i < server.args(); i++) {
    msg += server.argName(i) + ": " + server.arg(i) + "\n";
  }

  //pulse seems to be either 180 or 320 for my devices
  int pulse = server.arg("pulse").toInt();
  
  int code = server.arg("code").toInt();

  //bits 24 or 32
  int bits = server.arg("bits").toInt();

  mySwitch.setPulseLength(pulse);
  mySwitch.send(code, bits);
  
  server.send(200, "text/plain", msg);
  Serial.println(msg);
}

void loop() {
    // put your main code here, to run repeatedly:
    server.handleClient();

    
}
