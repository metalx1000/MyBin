/*
Copyright (C) 2020  Kris Occhipinti
https://filmsbykris.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino

//needed for library
#include <DNSServer.h>
#include <WiFiManager.h>         //https://github.com/tzapu/WiFiManager
#include <WiFiClient.h>
#include <ESP8266HTTPClient.h>

char url[] = "http://filmsbykris.com/scripts/2020/test_msg.php";

#include <Wire.h>               // Only needed for Arduino 1.6.5 and earlier
#include "SSD1306Wire.h"        // legacy: #include "SSD1306.h"
#include "images.h"


SSD1306Wire display(0x3c, D3, D5);  // ADDRESS, SDA, SCL  -  If not, they can be specified manually.

void setup() {
  Serial.begin(115200);

  WiFi.mode(WIFI_STA); // explicitly set mode, esp defaults to STA+AP
  WiFiManager wm;

  //reset settings - wipe credentials for testing
  //wm.resetSettings();

  bool res;
  res = wm.autoConnect("OLED"); // anonymous ap

  if (!res) {
    Serial.println("Failed to connect");
    // ESP.restart();
  }
  else {
    //if you get here you have connected to the WiFi
    Serial.println("connected...yeey :)");
  }

  // Initialising the UI will init the display too.
  display.init();
  display.flipScreenVertically();

}

void request(char *url) {
  WiFiClient client;

  HTTPClient http;

  Serial.print("[HTTP] begin...\n");
  if (http.begin(client, url)) {  // HTTP


    Serial.print("[HTTP] GET...\n");
    // start connection and send HTTP header
    int httpCode = http.GET();

    // httpCode will be negative on error
    if (httpCode > 0) {
      // HTTP header has been send and Server response header has been handled
      Serial.printf("[HTTP] GET... code: %d\n", httpCode);

      // file found at server
      if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
        String payload = http.getString();
        //wget message and scroll it across the screen (with added white space to allow leader and tail
        String msg = "                    " + payload + "                   ";
        int len = msg.length();
        for ( int i = 0; i < len; i++) {
          display.clear();
          display.setTextAlignment(TEXT_ALIGN_LEFT);
          display.setFont(ArialMT_Plain_24);
          display.drawString(0, 26, msg);
          msg.remove(0, 1);
          display.display();
          delay(100);
        }

      }
    } else {
      Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
      char msg[] = "[HTTP] GET... failed, error:";
      display.setFont(ArialMT_Plain_24);
      display.drawString(0, 26, msg);
    }

    http.end();
  } else {
    Serial.printf("[HTTP} Unable to connect\n");

  }

}

void doom() {
  // clear the display
  display.clear();
  display.drawXbm(16, 0, doom_width, doom_height, doom_bits);
  display.display();
}

void skull() {
  // clear the display
  display.clear();
  display.drawXbm(0, 0, skull_width, skull_height, skull_bits);
  display.display();
}

void loop() {
  // clear the display
  display.clear();

  doom();
  delay(5000);
  skull();
  delay(5000);
  request(url);
  delay(10);
}
