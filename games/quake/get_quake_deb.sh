#!/bin/bash
#Installs Quake

quake_url='https://lbry.tv/$/download/quake-xmen/908c331633ead5b412744f40c2f44f98adc37263'
clear

if [ "$(id -u)" != "0" ]; then
  echo "Sorry, you are not root or sudo."
  echo "Restarting as sudo."
  sleep 2

  sudo $0
  exit 1
fi

#install Quake Binaries
apt install quake

#apt update &&
#apt install quake ||
#echo "Error installing Quake" &&
#exit 1


#download game assests for quake and x-men
echo "Warning, please be sure own a copy of quake when using this script"
echo "X-Men: The Ravages of Apocalypse"
echo "A total Quake convertion"
echo -n "do you want to continue? (Y/n):"
read go

if [ "$go" = "y" ] || [ "$go" = "Y" ] || [ "$go" = "" ]
then
  mkdir -p /tmp/quake || exit 1
  cd /tmp/quake || exit 1

  wget -c "${quake_url}" -O quake-xmen.zip &&
    mkdir -p /usr/share/games/quake/id1/ &&
    unzip -o "quake-xmen.zip" -d /usr/share/games/quake/


  echo "======================================"
  echo "Please run the command 'quake' to play"
  echo "======================================"
  sleep 3

  echo "cd /usr/share/games/quake" > /usr/local/bin/xmenq &&
    echo "quakespasm -game xmen" >> /usr/local/bin/xmenq &&
    chmod +x /usr/local/bin/xmenq

  echo "======================================="
  echo "To run Xmen Mod run the command 'xmenq'"
  echo "======================================="
  sleep 3
fi

echo "Good Bye."
