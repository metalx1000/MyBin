// this draws lines on the screen with no Window
// gcc -Wall main.c -o main -lX11
//
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <unistd.h>

int main()
{
	Display* display = XOpenDisplay(NULL);
	Window root = DefaultRootWindow(display);
	GC gc = XCreateGC(display, root, 0, NULL);

  // red lines
  XSetForeground(display, gc, 0xFF0000);
  int m = 1000;
	for (int i = 0;i < m; i += 10) {
    XDrawLine(display, root, gc,50+i,50,m,m + i);
		XFlush(display);
		//usleep(20000);
	}
	
  // blue lines
  XSetForeground(display, gc, 0x0000FF);
  m = 1500;
	for (int i = m;i > 0; i -= 10) {
    XDrawLine(display, root, gc,1000,1000-i,m-i,m);
		XFlush(display);
		usleep(20000);
	}
	
  // green lines
  XSetForeground(display, gc, 0x00FF00);
  m = 2000;
	for (int i = 0;i < m; i += 10) {
    XDrawLine(display, root, gc,1500,1500-i,m-i,m);
		XFlush(display);
		usleep(20000);
	}
	

}

// XFillRectangle(display, d, gc, x, y, width, height)
//       Display *display;
//       Drawable d;
//       GC gc;
//       int x, y;
//       unsigned int width, height;
