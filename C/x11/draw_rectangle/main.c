// this draws a red box on teh screen with no Window
// gcc -Wall main.c -o main -lX11
//
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <unistd.h>

int main()
{
	Display* display = XOpenDisplay(NULL);
	Window root = DefaultRootWindow(display);
	GC gc = XCreateGC(display, root, 0, NULL);
	XSetForeground(display, gc, 0xFF0000);

	for (int counter = 0;; counter += 10) {
		XFillRectangle(display, root, gc, 50, 50, 500 + counter, 500);
		XFlush(display);
		usleep(20000);
	}
}

// XFillRectangle(display, d, gc, x, y, width, height)
//       Display *display;
//       Drawable d;
//       GC gc;
//       int x, y;
//       unsigned int width, height;
