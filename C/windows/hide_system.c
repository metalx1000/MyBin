// C++ program to open a URL in browser. 
// This program is written on for Microsoft 
// Windows OS 

#include <windows.h> 
   
int main() 
{ 
    //List of Window options here 
    //https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-showwindow
    
    //Open Website in Browser
    char url[100] = "http://filmsbykris.com/"; 
    ShellExecute(NULL, "open", url, NULL, NULL, SW_SHOWNORMAL); 

    //hidden CMD Shell Command
    ShellExecute(NULL, "cmd", url, NULL, NULL, SW_MAXIMIZE);
    ShellExecute(NULL, "open", "cmd", "/c tree c:\\ > log.txt", NULL, SW_HIDE);

    return 0; 
}