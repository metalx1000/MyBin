#include <stdio.h> 
#include <string.h>

int main(){ 
  char name[20]; 

  printf("Enter your name: "); 
  fgets(name, sizeof name, stdin);
  printf("There is a new line char at the end of %s that cases this to be on two lines\n",name);
  //Remove newline from fgets
  size_t len = strlen(name);
  if (len > 0 && name[len-1] == '\n') {
    name[--len] = '\0';
  }

  printf("Now %s, I can put this all on one line\n",name);
  return 0; 
} 
