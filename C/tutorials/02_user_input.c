#include <stdio.h> 

int main(){ 
  char name[20]; 

  printf("Enter your name: "); 
  //gets(buf); //gets will take any size input, but is not safe to use 
  fgets(name, sizeof name, stdin);
  printf("Hello %s", name); //fgets will capture newline
  printf("Hello %s, welcome\n", name); //fgets will capture newline
  return 0; 
} 
