#!/bin/bash

rm bin/*
for i in *.c
do
  echo "Compiling $i to bin/${i%%.c}"
  gcc "$i" -o bin/${i%%.c}
done
