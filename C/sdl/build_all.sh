#!/bin/bash

mkdir -p bin
for i in *.c
do
  name="${i%%.c}"
  gcc "$i" -c ; gcc "${name}.o" -o "bin/${name}" -lSDL
done
