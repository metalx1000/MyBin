#include <stdio.h> 
#include <stdlib.h>
#include <SDL/SDL.h>

int main(int argv, char **argc){
  SDL_Surface *display;
  SDL_Rect sDim;

  /*initialize SDL video subsystem*/
  if(SDL_Init(SDL_INIT_VIDEO) < 0){
    /*error, quit*/
    exit(-1);
  }

  /*retrieve 640 pixel wide by 480 pixel high 8 bit display with video memory access*/
  display = SDL_SetVideoMode(640, 480, 8, SDL_HWSURFACE);
  /*check that surface was retrieved*/
  if(display == NULL){
    /*quit SDL*/
    SDL_Quit();
    exit(-1);
  }
  
  /*set square dimensions and position*/
  sDim.w = 200;
  sDim.h = 200;
  sDim.x = 640/2;
  sDim.y = 480/2;

  /*draw square with given dimensions with color blue (0, 0, 255)*/
  SDL_FillRect(display, &sDim, SDL_MapRGB(display->format, 0, 0, 255));

  /*inform screen to update its contents*/
  SDL_UpdateRect(display, 0, 0, 0, 0);

  SDL_Delay(1000);

  /*set square dimensions and position*/
  sDim.w = 200;
  sDim.h = 200;
  sDim.x = 640/2;
  sDim.y = 480/2;

  /*draw square with given dimensions with color blue (0, 0, 255)*/
  SDL_FillRect(display, &sDim, SDL_MapRGB(display->format, 0, 200, 255));

  /*inform screen to update its contents*/
  SDL_UpdateRect(display, 0, 0, 0, 0);

  /*wait 5 seconds (5000ms) before exiting*/
  SDL_Delay(5000);

  SDL_Quit();

  exit(0);
}
