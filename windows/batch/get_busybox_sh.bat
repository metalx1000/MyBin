echo dim xHttp: Set xHttp = createobject("Microsoft.XMLHTTP") > go.vbs
echo dim bStrm: Set bStrm = createobject("Adodb.Stream") >> go.vbs
echo xHttp.Open "GET", "https://frippery.org/files/busybox/busybox.exe", False >> go.vbs
echo xHttp.Send >> go.vbs
echo with bStrm >> go.vbs
echo     .type = 1  >> go.vbs
echo     .open >> go.vbs
echo     .write xHttp.responseBody >> go.vbs
echo     .savetofile "busybox.exe", 2  >> go.vbs
echo end with >> go.vbs
cscript go.vbs
del go.vbs
busybox.exe sh
