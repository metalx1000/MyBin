@echo off
cls

'Download Busybox
echo dim xHttp: Set xHttp = createobject("Microsoft.XMLHTTP") > go.vbs
echo dim bStrm: Set bStrm = createobject("Adodb.Stream") >> go.vbs
echo xHttp.Open "GET", "https://frippery.org/files/busybox/busybox.exe", False >> go.vbs
echo xHttp.Send >> go.vbs
echo with bStrm >> go.vbs
echo     .type = 1  >> go.vbs
echo     .open >> go.vbs
echo     .write xHttp.responseBody >> go.vbs
echo     .savetofile "busybox.exe", 2  >> go.vbs
echo end with >> go.vbs
cscript go.vbs
del go.vbs

'download fzf
busybox.exe wget "https://github.com/junegunn/fzf-bin/releases/download/0.21.1/fzf-0.21.1-windows_386.zip" -O fzf.zip
busybox.exe unzip fzf.zip
busybox.exe wget "http://filmsbykris.com/scripts/2020/employee.lst" -qO-|fzf.exe
pause

'run a shell script from a url
busybox.exe wget -qO- "https://pastebin.com/raw/7Jc0G2Ys"|busybox.exe sh

'cleanup
del busybox.exe
del fzf.zip
del fzf.exe
