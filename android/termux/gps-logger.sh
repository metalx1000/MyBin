#!/bin/bash
#get's GPS location and TimeStamp and uploads it to a server

#change this to your server's url for logging
logger="https://filmsbykris.com/scripts/2020/echo.php"

#loop forever
while [ 1 ];
do
  #get GPS and add TIMESTAMP to JSON
  gps="$(termux-location|sed "s/\"latitude\"/\"timestamp\": $(date +%s),\n  \"latitude\"/g")"

  #upload JSON to server
  wget "$logger?gps=$gps" -qO-
done