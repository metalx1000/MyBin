#!/bin/bash
#get's GPS location and TimeStamp

#file where to save log
log="$HOME/gps.log"

#loop forever
while [ 1 ];
do
  #get GPS and add TIMESTAMP to JSON
  termux-location|sed "s/\"latitude\"/\"timestamp\": $(date +%s),\n  \"latitude\"/g" >> "$log"
  echo "," >> "$log"

done
