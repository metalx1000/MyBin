#!/bin/bash
###################################################################### 
#Copyright (C) 2020  Kris Occhipinti
#https://filmsbykris.com

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
###################################################################### 

function fail(){
  echo "Error..."
  exit 1
}

function push(){
  git add .
  git commit
  git push
}

function check_personal(){
  echo "metalx1000
  private
  personal
  shop
  598
  456"|while read string;
do
  grep -ir "$string" *
done|less
}

cd bin || fail
for i in *;
do 
  echo "Backing up /usr/local/bin/$i...";
  cp -v /usr/local/bin/$i .
done

check_personal
push


